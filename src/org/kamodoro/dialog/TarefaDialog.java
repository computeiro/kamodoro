package org.kamodoro.dialog;

import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.kamodoro.model.Prioridade;
import org.kamodoro.model.Tarefa;
import org.kamodoro.model.Tarefa.SituacaoTarefa;
import org.kamodoro.model.TarefaDAO;
import org.kamodoro.model.Usuario;
import org.kamodoro.model.UsuarioDAO;

public class TarefaDialog extends Dialog {
	private Composite content;
	
	private Text txTitulo;
	private ComboViewer cmbResponsavel;
	private ComboViewer cmbSituacao;
	private ComboViewer cmbPrioridade;
	private Text txDescricao;
	private Text txData;
	private Tarefa tarefa;
	
	private String popupTitulo;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public TarefaDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		content = (Composite) super.createDialogArea(parent);
		content.setLayout(new GridLayout(2, false));

		Color transparent = getShell().getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT);
		
		Label lbTitulo = new Label(content, SWT.None);
		lbTitulo.setText("T�tulo:");
		lbTitulo.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 2, 1));
		lbTitulo.setBackground(transparent);

		txTitulo = new Text(content, SWT.BORDER);
		txTitulo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Label lbDescricao = new Label(content, SWT.NONE);
		lbDescricao.setText("Descri��o");
		lbDescricao.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lbDescricao.setBackground(transparent);

		txDescricao = new Text(content, SWT.MULTI | SWT.WRAP | SWT.BORDER);
		txDescricao.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		Label lbResponsavel = new Label(content, SWT.NONE);
		lbResponsavel.setText("Respons�vel:");
		lbResponsavel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		lbResponsavel.setBackground(transparent);

		cmbResponsavel = new ComboViewer(content);
		cmbResponsavel.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Usuario) {
					Usuario usuario = (Usuario) element;
					return usuario.getNome();
				}
				return super.getText(element);
			}
		});

		cmbResponsavel.setContentProvider(ArrayContentProvider.getInstance());
		cmbResponsavel.setInput(UsuarioDAO.getUsuarios().toArray());
		cmbResponsavel.getCombo().select(0);

		Label lbPrioridade = new Label(content, SWT.NONE);
		lbPrioridade.setText("Prioridade:");
		lbPrioridade.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		lbPrioridade.setBackground(transparent);

		cmbPrioridade = new ComboViewer(content);
		cmbPrioridade.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object prioridade) {
				return prioridade.toString();
			}
		});
		
		cmbPrioridade.setContentProvider(ArrayContentProvider.getInstance());
		cmbPrioridade.setInput(Prioridade.values());
		cmbPrioridade.getCombo().select(0);
		cmbPrioridade.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();

				if (selection.size() > 0) {
					Prioridade prioridade = (Prioridade) selection.getFirstElement();
					setCorPrioridade(prioridade);
				}
			}
		});
		
		cmbPrioridade.getCombo().select(0);
		
		
		Label lbSituacao = new Label(content, SWT.NONE);
		lbSituacao.setText("Situa��o:");
		lbSituacao.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		lbSituacao.setBackground(transparent);
		
		cmbSituacao = new ComboViewer(content);
		cmbSituacao.setContentProvider(ArrayContentProvider.getInstance());
		cmbSituacao.setInput(SituacaoTarefa.values());
		cmbSituacao.getCombo().select(0);

		txData = new Text(content, SWT.READ_ONLY);
		txData.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 2, 1));
		txData.setBackground(transparent);

		if (tarefa == null) {
			tarefa = new Tarefa("");
		}

		preencherDados();

		return content;
	}

	private void preencherDados() {
		txTitulo.setText(tarefa.getTitulo());

		if (tarefa.getDescricao() != null) {
			txDescricao.setText(tarefa.getDescricao());
		}
		
		if(tarefa.getResponsavel() != null){
			cmbResponsavel.setSelection(new StructuredSelection(tarefa.getResponsavel()));
		}
		
		cmbPrioridade.setSelection(new StructuredSelection(tarefa.getPrioridade()));	
		cmbSituacao.setSelection(new StructuredSelection(tarefa.getSituacao()));	
		
		
		txData.setText(new SimpleDateFormat(Tarefa.DATE_FORMAT).format(tarefa.getData()));
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		buttonBar = parent;
		createButton(parent, IDialogConstants.OK_ID, "Salvar", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancelar", false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 500);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		if (popupTitulo == null) {
			popupTitulo = "Cadastro de Tarefas";
		}
		
		newShell.addShellListener(new ShellAdapter() {

            @Override
            public void shellActivated(ShellEvent shellevent) {
            	setCorPrioridade(tarefa.getPrioridade());
            }
        });
		
		newShell.setText(popupTitulo);
	}

	@Override
	protected void okPressed() {
		if (txTitulo.getText() != null && !"".equals(txTitulo.getText().trim())) {
			
			if(tarefa.getId() == null){
				tarefa.setId(TarefaDAO.getInstance().nextId());
			}
			
			tarefa.setTitulo(txTitulo.getText());
			tarefa.setDescricao(txDescricao.getText());

			IStructuredSelection selection = cmbResponsavel.getStructuredSelection();
			Usuario responsavel = (Usuario) selection.getFirstElement();
			tarefa.setResponsavel(responsavel);

			IStructuredSelection prioridadeSelect = cmbPrioridade.getStructuredSelection();
			Prioridade prioridade = (Prioridade) prioridadeSelect.getFirstElement();
			tarefa.setPrioridade(prioridade);
			
			IStructuredSelection situacaoSelect = cmbSituacao.getStructuredSelection();
			SituacaoTarefa situacao = (SituacaoTarefa) situacaoSelect.getFirstElement();
			tarefa.setSituacao(situacao);
			
			close();
		}
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	public void setPopupTitulo(String popupTitulo) {
		this.popupTitulo = popupTitulo;
	}
	
	public void setSituacao(SituacaoTarefa situacao){
		cmbSituacao.setSelection(new StructuredSelection(situacao));	
	}
	
	private void setCorPrioridade(Prioridade prioridade){
		RGB rgb = new RGB(prioridade.getRed(), prioridade.getGreen(), prioridade.getBlue());
		Color color = new Color(getShell().getDisplay(), rgb);
		
		content.setBackground(color);
	}

}
