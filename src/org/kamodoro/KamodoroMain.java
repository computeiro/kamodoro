package org.kamodoro;


import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.kamodoro.model.CRUDListener;
import org.kamodoro.model.Tarefa;
import org.kamodoro.model.Tarefa.SituacaoTarefa;
import org.kamodoro.model.TarefaDAO;
import org.kamodoro.ui.ListaTarefasCmp;
import org.kamodoro.ui.PomodoroTimer;

public class KamodoroMain extends ApplicationWindow {
	private ListaTarefasCmp planejadasLista;
	private ListaTarefasCmp executandoLista;
	private ListaTarefasCmp concluidasLista;

	/**
	 * Create the application window.
	 */
	public KamodoroMain() {
		super(null);
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		Color branco = getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE);
		parent.setBackground(branco);
		
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(3, true));
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setBackground(branco);
		
		planejadasLista = new ListaTarefasCmp(container, "Planejadas", SituacaoTarefa.Planejada);
		planejadasLista.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		executandoLista = new ListaTarefasCmp(container, "Executando", SituacaoTarefa.Executando);
		executandoLista.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		executandoLista.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		concluidasLista = new ListaTarefasCmp(container,  "Conclu�das", SituacaoTarefa.Conclu�da);
		concluidasLista.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		concluidasLista.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		CRUDListener crudListener = new CrudListenerImpl(this);
		
		TarefaDAO.getInstance().setCrudListener(crudListener);
		
		recarregarTarefas();
		
		return container;
	}
	
	private void recarregarTarefas(){
		planejadasLista.refresh();
		executandoLista.refresh();
		concluidasLista.refresh();
	}
	

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		return menuManager;
	}

	/**
	 * Create the toolbar manager.
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		ToolBarManager toolBarManager = new ToolBarManager(style);
		return toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			KamodoroMain window = new KamodoroMain();
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Kamodoro Task Manager");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 600);
	}
	
	private class CrudListenerImpl implements CRUDListener{
		private KamodoroMain app;
		
		public CrudListenerImpl(KamodoroMain app) {
			this.app = app;
		}
		
		@Override
		public void afterSave(Tarefa tarefa) {
			app.recarregarTarefas();
		}

		@Override
		public void afterDelete(Tarefa tarefa) {
			app.recarregarTarefas();
		}

		@Override
		public boolean beforeDelete(Tarefa tarefa) {
			StringBuilder descr = new StringBuilder();
			descr.append("Tem certeza que gostaria de remover a tarefa: '");
			descr.append(tarefa.getTitulo());
			descr.append("' ?");
			
			boolean deletar = MessageDialog.openConfirm(app.getShell(), "Deletar tarefa", descr.toString());
			
			if(deletar){
				PomodoroTimer pomodoro = PomodoroTimer.getInstance();
				
				if(pomodoro.getTarefa().getId() == tarefa.getId()){
					pomodoro.finalizar();
				}
			}
			
			return deletar;
		}
		
	}
	
	@Override
	public boolean close() {
		PomodoroTimer.getInstance().finalizar();
		return super.close();
	}

}
