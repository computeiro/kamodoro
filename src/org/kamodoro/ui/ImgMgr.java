package org.kamodoro.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public enum ImgMgr {
	
	Dave_Foto("dave.png"), 
	Kevin_Foto("kevin.png"), 
	Stuart_Foto("stuart.png"), 
	Ningem_Foto("ninguem.png"),
	ICO_mais("mais.png"),
	ICO_lixo("lixo.png"),
	ICO_pomodoro("pomodoro.png"),
	IMG_pomodoro("pomodoro32.png"),
	ICO_copiar("copiar.png"),
	ICO_editar("editar.png"),
	ICO_interrupcao("interrupcao.png"),
	ICO_play("play.png"),
	ICO_pause("pause.png");
	
	private ImageDescriptor descriptor;
	
	private ImgMgr(String arquivo){
		descriptor = ImageDescriptor.createFromURL(ImgMgr.class.getResource("img/" + arquivo)); 
	}
	
	public ImageDescriptor getImageDescriptor(String imgName) {
		return descriptor;
	}

	public Image getImage() {
		return descriptor.createImage();
	}

}
