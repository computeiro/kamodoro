package org.kamodoro.ui;

import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.kamodoro.dialog.TarefaDialog;
import org.kamodoro.model.Prioridade;
import org.kamodoro.model.Tarefa;
import org.kamodoro.model.Tarefa.SituacaoTarefa;
import org.kamodoro.model.TarefaDAO;
import org.kamodoro.model.Usuario;
import org.kamodoro.ui.PomodoroTimer.ClockListener;

public class CartaoTarefaCmp extends Composite {
	private static final Usuario USUARIO_NINGUEM = new Usuario("Ningu�m", ImgMgr.Ningem_Foto.getImage());

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Tarefa tarefa;

	public CartaoTarefaCmp(Composite parent, Tarefa tarefa, boolean emPomodoro) {
		super(parent, SWT.NONE);
		this.tarefa = tarefa;

		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
			}
		});
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);

		setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));

		GridLayout layout = new GridLayout(1, false);
		layout.verticalSpacing = 0;
		setLayout(layout);

		Composite buttonBar = new Composite(this, SWT.RIGHT_TO_LEFT);
		buttonBar.setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
		buttonBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		buttonBar.setLayout(new RowLayout(SWT.HORIZONTAL));

		Button btnEditar = new Button(buttonBar, SWT.None);
		btnEditar.setImage(ImgMgr.ICO_editar.getImage());
		btnEditar.setToolTipText("Editar tarefa");
		btnEditar.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				editarTarefa();
			}
		});

		Button btnCopy = new Button(buttonBar, SWT.None);
		btnCopy.setImage(ImgMgr.ICO_copiar.getImage());
		btnCopy.setToolTipText("Copiar como texto");
		btnCopy.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				Clipboard clipboard = new Clipboard(getDisplay());
				TextTransfer textTransfer = TextTransfer.getInstance();
				clipboard.setContents(new String[] {tarefa.getConteudoFormatado()},	new Transfer[] { textTransfer });
				clipboard.dispose();
			}
		});

		Button btnPomodoro = new Button(buttonBar, SWT.None);
		btnPomodoro.setImage(ImgMgr.ICO_pomodoro.getImage());
		btnPomodoro.setToolTipText("Iniciar pomodoro");
		
		btnPomodoro.addListener(SWT.Selection, (event) -> {
			if(tarefa.getSituacao() == SituacaoTarefa.Conclu�da){
				MessageDialog.openWarning(getShell(), "N�o � poss�vel iniciar pomodoro!", 
						"Esta tarefa j� foi conclu�da e n�o pode ser inicializado um pomodoro.");
				return;
			}
			
			PomodoroTimer pomodoro = PomodoroTimer.getInstance();
			
			if(pomodoro.isActive()){
				StringBuilder msg = new StringBuilder();
				msg.append("Est� em andamento um pomodoro para a tarefa: '");
				msg.append(pomodoro.getTarefa().getTitulo());
				msg.append("'.\n\nVoc� quer cancelar o pomodoro em andamento e inicializar um novo para a tarefa '");
				msg.append(tarefa.getTitulo());
				msg.append("'?");
				
				boolean continuar = MessageDialog.openConfirm(getShell(), "J� existe um pomodoro aberto", msg.toString()); 
				
				
				if(! continuar){
					return;
				}
			}
			
			pomodoro.inicia(tarefa);
			//Se abrimos um pomodoro, ent�o colocamos a tarefa como Executando (estando ou n�o)
			tarefa.setSituacao(SituacaoTarefa.Executando);
			TarefaDAO.getInstance().salvar(tarefa);
		});

		Button btnExcluir = new Button(buttonBar, SWT.None);
		btnExcluir.setImage(ImgMgr.ICO_lixo.getImage());
		btnExcluir.setToolTipText("Apagar tarefa");
		
		btnExcluir.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				TarefaDAO.getInstance().deletar(tarefa);
			}
		});
		
		Composite cardCmp = new Composite(this, SWT.BORDER);
		cardCmp.setLayout(new GridLayout(2, false));
		cardCmp.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Prioridade p = tarefa.getPrioridade();
		RGB rgbPrioridade = new RGB(p.getRed(), p.getGreen(), p.getBlue());
		
		cardCmp.setBackground(new Color(getDisplay(), rgbPrioridade));
		
		Label lbImagemUsuario = new Label(cardCmp, SWT.BORDER|SWT.SHADOW_IN);
		lbImagemUsuario.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 2));
		Usuario responsavel = tarefa.getResponsavel();

		if (responsavel == null) {
			responsavel = USUARIO_NINGUEM;
		}

		lbImagemUsuario.setImage(responsavel.getFoto());
		lbImagemUsuario.setToolTipText(responsavel.getNome());

		Label lbTitulo = new Label(cardCmp, SWT.BOLD);
		lbTitulo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		lbTitulo.setText(tarefa.getTitulo());
		lbTitulo.setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));

		FontDescriptor fontDescr = FontDescriptor.createFrom(lbTitulo.getFont());
		fontDescr = fontDescr.setHeight(12);
		Font boldFont = fontDescr.createFont(lbTitulo.getDisplay());
		lbTitulo.setFont(boldFont);

		Label lbDate = new Label(cardCmp, SWT.None);
		lbDate.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, false));
		lbDate.setText(new SimpleDateFormat(Tarefa.DATE_FORMAT).format(tarefa.getData()));
		lbDate.setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
		
		
		MouseListener doubleClickListener = new CardDoubleClickListener();
		
		lbDate.addMouseListener(doubleClickListener);
		lbTitulo.addMouseListener(doubleClickListener);
		lbImagemUsuario.addMouseListener(doubleClickListener);
		this.addMouseListener(doubleClickListener);
		
		if(emPomodoro){
			criaPomodoroCmp();
		}
		
	}
	
	private void criaPomodoroCmp(){
		Font fontMono = new Font(getDisplay(), "Monospaced", 10, SWT.NONE);
		
		PomodoroTimer pomodoro = PomodoroTimer.getInstance();
		
		Composite pomodoroCmp = new Composite(this, SWT.None);
		pomodoroCmp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		pomodoroCmp.setLayout(new GridLayout(5, false));
		
		Label lbImgPomodoro = new Label(pomodoroCmp, SWT.NONE);
		lbImgPomodoro.setImage(ImgMgr.IMG_pomodoro.getImage());
		
		Text txContador = new Text(pomodoroCmp, SWT.READ_ONLY | SWT.BORDER |SWT.RIGHT_TO_LEFT);
		txContador.setFont(fontMono);
		txContador.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		final Button btnPause = new Button(pomodoroCmp, SWT.NONE);
		btnPause.setImage(ImgMgr.ICO_pause.getImage());
		btnPause.setToolTipText("Pausar pomodoro");
		
		btnPause.addListener(SWT.Selection, event -> {
			pomodoro.togglePause();
			
			if(pomodoro.isPausado()){
				btnPause.setImage(ImgMgr.ICO_play.getImage());
				btnPause.setToolTipText("Continuar pomodoro");
			}else{
				btnPause.setImage(ImgMgr.ICO_pause.getImage());
				btnPause.setToolTipText("Pausar pomodoro");
			}
			
			
		});
		
		Text txInterrupcoes = new Text(pomodoroCmp, SWT.READ_ONLY | SWT.BORDER | SWT.RIGHT_TO_LEFT);
		txInterrupcoes.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		txInterrupcoes.setFont(fontMono);
		txInterrupcoes.setText("   0");
		
		Button btnAddInterrupcao = new Button(pomodoroCmp, SWT.NONE);
		btnAddInterrupcao.setImage(ImgMgr.ICO_interrupcao.getImage());
		btnAddInterrupcao.setToolTipText("Incrementar interrup��es");
		btnAddInterrupcao.addListener(SWT.Selection, (event)->{
			
			if(pomodoro.isActive()){
				pomodoro.addInterrupcao();
				
				txInterrupcoes.setText(String.valueOf(pomodoro.getInterrupcoes()));
			}else{
				MessageDialog.openInformation(getShell(), "Pomodo n�o est� ativo!", "Voc� s� pode incrementar contagem de interrup��es se o pomodoro estiver ativo.");
			}
		});

		pomodoro.setClockListener(new ClockListener() {
			
			@Override
			public void onTick(String displaybleTime) {
				getDisplay().asyncExec(()->{
					txContador.setText(displaybleTime);
				});
			}
			
			@Override
			public void onFinish() {
				//Testa se a aplica��o n�o foi finalizada
				if(! getShell().isDisposed()){
					StringBuilder msg = new StringBuilder();
					msg.append("Tempo encerrado para o Pomodoro da tarefa '");
					msg.append(pomodoro.getTarefa().getTitulo());
					msg.append("' !");
				
					MessageDialog.openInformation(getShell(), "Pomodoro finalizado!", msg.toString());
				}
			
				
			}
		});
		
	
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	private void editarTarefa() {
		TarefaDialog dialog = new TarefaDialog(getShell());
		dialog.setTarefa(tarefa);
		dialog.setPopupTitulo("Editar tarefa");

		if (dialog.open() == Window.OK) {
			TarefaDAO.getInstance().salvar(dialog.getTarefa());
		}
	}

	private class CardDoubleClickListener implements MouseListener {

		@Override
		public void mouseDoubleClick(MouseEvent arg0) {
			editarTarefa();
		}

		@Override
		public void mouseDown(MouseEvent arg0) {
		}

		@Override
		public void mouseUp(MouseEvent arg0) {
		}

	}

}
