package org.kamodoro.ui;

import org.kamodoro.model.Tarefa;

public class PomodoroTimer {
	private static final int LIMITE_25_MINUTOS = 1000 * 60 * 60 * 25;
	private static PomodoroTimer instance;
	
	private Tarefa tarefa;
	private long startTime;
	private long baseTime;
	private boolean pause;
	private int interrupcoes;
	private ClockListener  clockListener;
	private String displayTime;
	
	private Thread contador;
	
	private PomodoroTimer(){
	}
	
	public static PomodoroTimer getInstance(){
		if(instance == null){
			instance = new PomodoroTimer();
		}
		
		return instance;
	}
	
	public boolean isActive(){
		return contador != null && contador.isAlive();
	}
	
	public synchronized void inicia(Tarefa tarefa){
		this.tarefa = tarefa;
		this.clockListener = null;
		
		if(contador != null && contador.isAlive()){
			//Cancelando Thread anterior
			finalizar();
		}
		
		this.contador = new Thread(()->{
			startTime = System.currentTimeMillis();
			baseTime = startTime;
			boolean interrompido = false;
			boolean acabouContagem = false;
			
			do{
				try{
					Thread.sleep(500);
					
					if(pause){
						baseTime += 500;
					}else{
						if(clockListener != null){
							clockListener.onTick(getDisplayTime());
						}
					}
				}catch(InterruptedException ignored){
					interrompido = true;
				}
				
				acabouContagem = System.currentTimeMillis() > (baseTime + LIMITE_25_MINUTOS);
				
			}while(!interrompido &&  !acabouContagem);
			
			
			System.out.println("Pomodoro acabou!");
			
			if(clockListener != null){
				clockListener.onFinish();
			}
		}, "Pomodoro: Tarefa " + tarefa.getId());
		
		instance.contador.start();
	}
	
	public void togglePause(){
		this.pause = !pause;
	}
	
	public boolean isPausado(){
		return pause;
	}
	
	public void setClockListener(ClockListener clockListener) {
		this.clockListener = clockListener;
	}

	public Tarefa getTarefa(){
		return tarefa;
	}
	
	private String getDisplayTime(){
		
		if(! pause){
			long diff = Math.abs(System.currentTimeMillis() - baseTime - LIMITE_25_MINUTOS);

			long minutos =  Math.round(diff / (1000 * 60 * 60));
			long segundos = Math.round( Math.floorMod(diff/1000, 60));
			
			StringBuilder stb = new StringBuilder();
			stb.append(minutos);
			stb.append(":");
			
			if(segundos < 10){
				stb.append("0");
			}
			
			stb.append(segundos);
			
			displayTime = stb.toString();
		}
		
		return displayTime;
		
		
	}
	
	public void addInterrupcao(){
		interrupcoes++;
	}
	
	public int getInterrupcoes(){
		return interrupcoes;
	}
	
	
	public interface ClockListener {
		public void onTick(String displaybleTime);
		public void onFinish();
	}


	public void finalizar() {
		try{
			contador.interrupt();
			tarefa = null;
		}catch(Exception ignored){
		}
	}
	
}
