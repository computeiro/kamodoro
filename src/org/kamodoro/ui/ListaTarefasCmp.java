package org.kamodoro.ui;

import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.kamodoro.dialog.TarefaDialog;
import org.kamodoro.model.Tarefa;
import org.kamodoro.model.Tarefa.SituacaoTarefa;
import org.kamodoro.model.TarefaDAO;

public class ListaTarefasCmp extends Composite {
	private Text filtroTitulo;
	private Composite tarefasContainer;
	private SituacaoTarefa filtroSituacao;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ListaTarefasCmp(Composite parent, String titulo, SituacaoTarefa filtroSituacao) {
		super(parent, SWT.BORDER);
		this.filtroSituacao = filtroSituacao;
		this.setLayout(new GridLayout(2, false));
		
		setBackground(new Color(getDisplay(), new RGB(250, 250, 250)));
		
		setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
		
		
		Label lbTitulo = new Label(this, SWT.NONE);
		lbTitulo.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		lbTitulo.setText(titulo);
		lbTitulo.setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
		
		//Modificando a fonte do t�tulo
		FontDescriptor fontDescr = FontDescriptor.createFrom(lbTitulo.getFont());
		fontDescr = fontDescr.setStyle(SWT.BOLD);
		fontDescr = fontDescr.setHeight(12);
		
		Font boldFont = fontDescr.createFont(lbTitulo.getDisplay());
		lbTitulo.setFont(boldFont);
		
		filtroTitulo = new Text(this, SWT.BORDER);
		filtroTitulo.setMessage("Filtrar tarefas...");
		filtroTitulo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		filtroTitulo.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent arg0) {
				refresh();
			}
		});
		
		Button btnAdicionarTarefa = new Button(this, SWT.NONE);
		btnAdicionarTarefa.setImage(ImgMgr.ICO_mais.getImage());
		btnAdicionarTarefa.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TarefaDialog dialog = new TarefaDialog(getShell());
				
				if(dialog.open() == Window.OK){
					TarefaDAO.getInstance().salvar(dialog.getTarefa());
				}
			}

		});
		
		tarefasContainer = new Composite(this, SWT.None);
		tarefasContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		GridLayout layoutTarefas = new GridLayout(1, true);
		layoutTarefas.verticalSpacing = 10;
		
		tarefasContainer.setLayout(layoutTarefas);
		tarefasContainer.setBackground(getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
	}
	
	
	public void refresh() {
		for(Control c : tarefasContainer.getChildren()){
			c.dispose();
		}
		
		final PomodoroTimer pomodoro = PomodoroTimer.getInstance();
		
		Comparator<Tarefa> comparatorTarefas =  new Comparator<Tarefa>() {

			@Override
			public int compare(Tarefa t1, Tarefa t2) {
				if(pomodoro.isActive()){
					if(t1.getId() == pomodoro.getTarefa().getId()){
						return -1;
					}
					
					if(t2.getId() == pomodoro.getTarefa().getId()){
						return 1;
					}
				}
				
				return t1.getData().compareTo(t2.getData());
			}
			
		};
		
		List<Tarefa> tarefas = TarefaDAO.getInstance().getTarefas(comparatorTarefas);
		
		for(Tarefa tarefa : tarefas){
			
			boolean emPomodoro = false; 
			
			if(pomodoro.isActive() ){
				emPomodoro = tarefa.getId().equals(pomodoro.getTarefa().getId());
			}
			
			if(tarefa.getSituacao() != filtroSituacao){
				continue;
			}
			
			String filtro = filtroTitulo.getText().trim().toUpperCase();
			
			
			if(!"".equals(filtro) && !tarefa.getTitulo().toUpperCase().contains(filtro)){
				continue;
			}
			
			CartaoTarefaCmp cartao = new CartaoTarefaCmp(tarefasContainer, tarefa, emPomodoro);
			cartao.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			tarefasContainer.layout();
			
			cartao.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseUp(MouseEvent arg0) {
				}
				
				@Override
				public void mouseDown(MouseEvent arg0) {
				}
				
				@Override
				public void mouseDoubleClick(MouseEvent arg0) {
					TarefaDialog dialog = new TarefaDialog(getShell());
					dialog.setTarefa(tarefa);
					
					if(dialog.open() == Window.OK){
						TarefaDAO.getInstance().salvar(dialog.getTarefa());
					}
					
				}
			});
			
			this.layout();
		}
	}
}
