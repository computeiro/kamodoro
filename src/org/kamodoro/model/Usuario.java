package org.kamodoro.model;

import org.eclipse.swt.graphics.Image;

public class Usuario {
	private String nome;
	private Image foto;

	public Usuario(String nome, Image foto){
		this.nome = nome;
		this.foto = foto;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Image getFoto() {
		return foto;
	}
	public void setFoto(Image foto) {
		this.foto = foto;
	}
}
