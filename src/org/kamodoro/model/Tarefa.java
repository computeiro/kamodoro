package org.kamodoro.model;

import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

public class Tarefa {
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
	
	private Integer id;
	private String titulo;
	private String descricao;
	private Usuario responsavel;
	private Date data;
	private SituacaoTarefa status;
	private Prioridade prioridade;
	
	public Tarefa(String titulo){
		//Devria ser a PK do Banco de Dados
		this.titulo = titulo;
		this.status = SituacaoTarefa.Planejada;
		this.prioridade = Prioridade.M�dia;
		this.data = new Date();
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public SituacaoTarefa getSituacao() {
		return status;
	}

	public void setSituacao(SituacaoTarefa status) {
		this.status = status;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public Prioridade getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}
	
	public Usuario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	
	//Enum s�o constantes com mais eleg�ncia e poder
	public enum SituacaoTarefa{
		Planejada, Executando, Conclu�da; 
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tarefa){
			return this.id == ((Tarefa) obj).getId();
		}
		
		 return false;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getConteudoFormatado() {
		StringBuilder txt = new StringBuilder();
		txt.append("Tarefa: ");
		txt.append(titulo);
		
		txt.append("\nPrioridade: ");
		txt.append(prioridade.toString());
		
		txt.append("\nData: ");
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		txt.append(sdf.format(data));
		
		if(responsavel != null){
			txt.append("\nRespons�vel: ");
			txt.append(responsavel);
		}
		
		if(descricao != null){
			txt.append("\nDescri��o:\n ");
			txt.append(descricao);
		}
		
		return txt.toString();
	}
}
