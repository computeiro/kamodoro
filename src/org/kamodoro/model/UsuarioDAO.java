package org.kamodoro.model;

import java.util.ArrayList;
import java.util.List;

import org.kamodoro.ui.ImgMgr;

public class UsuarioDAO {
	
	
	public static List<Usuario> getUsuarios(){
		List<Usuario> usuarios = new ArrayList<>();

		usuarios.add(new Usuario("Indefinido", ImgMgr.Ningem_Foto.getImage()));
		usuarios.add(new Usuario("Kevin", ImgMgr.Kevin_Foto.getImage()));
		usuarios.add(new Usuario("Stuart", ImgMgr.Stuart_Foto.getImage()));
		usuarios.add(new Usuario("Dave", ImgMgr.Dave_Foto.getImage()));
		return usuarios;
		
		
	}
}
