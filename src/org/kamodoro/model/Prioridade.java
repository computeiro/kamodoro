package org.kamodoro.model;

public enum Prioridade {
	Baixa(153, 255, 153),
	M�dia(255, 255, 0),
	Alta(255, 153, 153);

	private Prioridade(int r, int g, int b) {
		this.red = r;
		this.green = g;
		this.blue = b;
	}

	private int red;
	private int green;
	private int blue;

	public int getRed() {
		return red;
	}

	public int getGreen() {
		return green;
	}

	public int getBlue() {
		return blue;
	}
}
