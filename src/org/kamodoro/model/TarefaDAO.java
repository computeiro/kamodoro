package org.kamodoro.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.kamodoro.model.Tarefa.SituacaoTarefa;
import org.kamodoro.ui.ImgMgr;

//Essa seria uma camada entre a aplica��o e o banco de dados
public class TarefaDAO {
	private static TarefaDAO instance;

	private List<Tarefa> tarefas;
	private CRUDListener crudListener;
	private int nextID;
	
	public static TarefaDAO getInstance(){
		if(instance == null){
			instance = new TarefaDAO();
		}
		
		return instance;
	}
	
	
	private TarefaDAO(){
		tarefas = new ArrayList<>();
		
		Usuario dave = new Usuario("Dave", ImgMgr.Dave_Foto.getImage());
		Usuario kevin = new Usuario("Kevin", ImgMgr.Kevin_Foto.getImage());
		Usuario stuart = new Usuario("Stuart", ImgMgr.Stuart_Foto.getImage());
		
		//Tarefa 1
		Tarefa tarefaOct = new Tarefa("Revisar octanagem");
		tarefaOct.setPrioridade(Prioridade.Alta);
		tarefaOct.setDescricao("Revisar a f�rmula das bebidas do servi�o de bordo");
		tarefaOct.setResponsavel(dave);
		tarefaOct.setSituacao(SituacaoTarefa.Conclu�da);
		
		tarefaOct.setId(nextId());
		tarefas.add(tarefaOct);
		
		//Tarefa 2
		Tarefa tarefaPc = new Tarefa("Configurar computador da Nave");
		tarefaPc.setPrioridade(Prioridade.M�dia);
		tarefaPc.setDescricao("Formatar PC da Nave.\nInstalar programas emerg�nciais para viagens interplanet�rias: Starcraft II, CS GO e Dota2.");
		tarefaPc.setSituacao(SituacaoTarefa.Executando);
		tarefaPc.setResponsavel(kevin);
		
		tarefaPc.setId(nextId());
		tarefas.add(tarefaPc);
		
		//Tarefa 3
		Tarefa tarefaSeds = new Tarefa("Implementar SEDS");
		tarefaSeds.setPrioridade(Prioridade.M�dia);
		tarefaSeds.setDescricao("SEDS: Sensor de Enventos Dignos de Sefies.\nN�o queremos perder a change de uma selfie com um cometa passando, por exemplo.");
		tarefaSeds.setSituacao(SituacaoTarefa.Executando);
		tarefaSeds.setResponsavel(stuart);
		
		tarefaSeds.setId(nextId());
		tarefas.add(tarefaSeds);
		
		//Tarefa 4
		Tarefa tarefaMalas = new Tarefa("Embarcar as malas");
		tarefaMalas.setPrioridade(Prioridade.Baixa);
		tarefaMalas.setDescricao("Distribuir o checklist aos tripulantes para que levem os itens adequados � moda outono/inverno em Marte.");
		
		tarefaMalas.setId(nextId());
		tarefas.add(tarefaMalas);
		
		//Tarefa 5
		Tarefa tarefaLouca = new Tarefa("Lavar Lou�a");
		tarefaLouca.setPrioridade(Prioridade.Alta);
		tarefaLouca.setDescricao("Pegar a bucha, encharcar com �gua, pingar o detergente e esfregar generosamente em cada prato da pia.");
		
		tarefaLouca.setId(nextId());
		tarefas.add(tarefaLouca);
	}
	
	//Isso deveria vir do Banco de dados
	public List<Tarefa> getTarefas(){
		return getTarefas(null);
	}
	
	public List<Tarefa> getTarefas(Comparator<Tarefa> comparator){
		synchronized(tarefas){
			if(comparator != null){
				Collections.sort(tarefas, comparator);
			}
			
			return Collections.unmodifiableList(tarefas);
		}
	}
	
	public void deletar(Tarefa tarefa){
		synchronized(tarefas){
			if(! crudListener.beforeDelete(tarefa)){
				return;
			}
			
			if(tarefas.remove(tarefa)){
				crudListener.afterDelete(tarefa);
			}
		}
	}
	
	public void salvar(Tarefa tarefa){
		synchronized(tarefas){
			if(! tarefas.contains(tarefa)){
				tarefas.add(tarefa);
			}
			
			crudListener.afterSave(tarefa);
		}
	}


	public synchronized int nextId() {
		nextID++;
		return nextID;
	}


	public void setCrudListener(CRUDListener crudListener) {
		this.crudListener = crudListener;
	}
	
}
