package org.kamodoro.model;


//CRUD - Create (criar), Read (ler/obter) Update (atualizar), Delete (remover)
//Listener -> Ouvir (uma a��o)
public interface CRUDListener {
	
	public void afterSave(Tarefa tarefa);
	public void afterDelete(Tarefa tarefa);
	public boolean beforeDelete(Tarefa tarefa);
	
}
